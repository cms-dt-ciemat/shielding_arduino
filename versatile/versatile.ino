///////////////////////
/// GENERAL OPTIONS ///
///////////////////////

/// Select board type
//#define RAMPS
#define TMC262  /// TMC262 admits 2 different pinouts
//#define TMC262_ORIG
#define TMC262_PCB


/// Motion configuration values
#ifdef RAMPS
  #define SETPOINT_FAST 400 /// in rpm
  #define SETPOINT_SLOW 200 /// in rpm
  #define VEL_CHANGE_INTERVAL 500 /// in us time between variations of 1 rpm in velocity
  #define DECEL_MUL 10 /// how much faster is deceleration than acceleration
  #define JOG 100 /// Start/stop movement directly to/from this speed is safe
  #define MICROSTEPPING 4
#endif

#ifdef TMC262
  #define SETPOINT_FAST 500 /// in rpm
  #define SETPOINT_SLOW 250 /// in rpm
  #define VEL_CHANGE_INTERVAL 500 /// in us time between variations of 1 rpm in velocity
  #define DECEL_MUL 3 /// how much faster is deceleration than acceleration
  #define JOG 100 /// Start/stop movement directly to/from this speed is safe
  #define MICROSTEPPING 1
#endif

#define ENABLE_ENDSTOPS
#define ENCODER_ERROR_THRESHOLD 100

/// serial report inteval in seconds (0 disable)
/// disable for production because it produces a glitch in the motion
#define SERIAL_INTERVAL 0

/// System parameters
#define GEAR_RATIO 7
#define ENCODER_STEPS_PER_REV 32
#define MOTOR_STEPS (200*MICROSTEPPING)


/////////////////
/// LIBRARIES ///
/////////////////
#ifdef TMC262
  #include <SPI.h>
  #include "TMC26XStepperMOD.h"
#endif

/////////////////////////////
/// INPUTS & CONTROL PINS ///
/////////////////////////////

#ifdef RAMPS
  /// MANDO: ACTIVE LOW MENOS LA SETA, QUE ES ACTIVE HIGH
  #define COMMON_MANDO          16 
  #define OUT_FAST              17
  #define OUT_SLOW              23
  #define IN_SLOW               25
  #define IN_FAST               27
  #define SETA_MANDO            29
  /// CAJA      
  #define DISABLE_MOTOR1        63  /// ACTIVE LOW
  #define DISABLE_MOTOR0        40  /// ACTIVE LOW
  #define DIS_MOT_INPUT_MODE    INPUT_PULLUP
  #define DIS_MOT_ACTIVE_LOW    true
  /// SENSORES        
  #define ENDSTOP_1_IN          32 /// IZQUIERDO - ACTIVE LOW
  #define ENDSTOP_1_OUT         47 /// IZQUIERDO - ACTIVE LOW
  #define ENCODER_1_X           45
  #define ENCODER_1_Y           43
  #define ENCODER_0_Y           41
  #define ENCODER_0_X           39
  #define ENDSTOP_0_IN          35 /// DERECHO   - ACTIVE LOW
  #define ENDSTOP_0_OUT         37 /// DERECHO   - ACTIVE LOW
  /// Motor #0 pins (Derecho) 
  #define DRIVER0_STEP_PIN      26
  #define DRIVER0_DIR_PIN       28
  #define DRIVER0_ENABLE_PIN    24
  /// Motor #1 pins (Izquierdo)
  #define DRIVER1_STEP_PIN      36
  #define DRIVER1_DIR_PIN       34
  #define DRIVER1_ENABLE_PIN    30
  /// Driver parameters
  #define DRIVER_ENABLE_ACTLOW  true
#endif

#ifdef TMC262
  /// MANDO: ACTIVE LOW MENOS LA SETA, QUE ES ACTIVE HIGH
  #define IN_FAST               4
  #define IN_SLOW               5
  #define OUT_SLOW              6
  #define OUT_FAST              7
  #define SETA_MANDO            3
  /// CAJA        
  #define DISABLE_MOTOR1        23
  #define DISABLE_MOTOR0        22
  #define DIS_MOT_INPUT_MODE    INPUT
  #define DIS_MOT_ACTIVE_LOW    false
  /// SENSORES        
  #ifdef TMC262_ORIG
    #define ENDSTOP_1_IN          25 /// IZQUIERDO - ACTIVE LOW
    #define ENDSTOP_1_OUT         27 /// IZQUIERDO - ACTIVE LOW
    #define ENDSTOP_0_IN          39 /// DERECHO   - ACTIVE LOW
    #define ENDSTOP_0_OUT         41 /// DERECHO   - ACTIVE LOW
  #endif
  #ifdef TMC262_PCB
    #define ENDSTOP_1_IN          38 /// IZQUIERDO - ACTIVE LOW
    #define ENDSTOP_1_OUT         40 /// IZQUIERDO - ACTIVE LOW
    #define ENDSTOP_0_IN          34 /// DERECHO   - ACTIVE LOW
    #define ENDSTOP_0_OUT         36 /// DERECHO   - ACTIVE LOW
  #endif
  #define ENCODER_1_X           A0
  #define ENCODER_1_Y           A1
  #define ENCODER_0_X           A2
  #define ENCODER_0_Y           A3
  /// Motor #0 pins (Derecho) 
  #define DRIVER0_CS_PIN        43
  #define DRIVER0_STEP_PIN      31 /// Not used but needed for constructor. Set to floating pin
  #define DRIVER0_DIR_PIN       32 /// Not used but needed for constructor. Set to floating pin
  #define DRIVER0_ENABLE_PIN    44
  #define DRIVER0_SGTST         45
  /// Motor #1 pins (Izquierdo)
  #define DRIVER1_CS_PIN        46
  #define DRIVER1_STEP_PIN      33 /// Not used but needed for constructor. Set to floating pin
  #define DRIVER1_DIR_PIN       26 /// Not used but needed for constructor. Set to floating pin
  #define DRIVER1_ENABLE_PIN    47
  #define DRIVER1_SGTST         48
  /// Driver parameters
  #define DRIVER_ENABLE_ACTLOW  true
  #define DRIVER_SIGN           -1
  #define RESISTOR 75 /// Driver's current sense resistors
  #define REST_CURRENT 200 ///in mA
  #define MOVE_CURRENT 2000 ///in mA
#endif

/// Constant vars to iterate on motors/sensors
byte MOTOR_ENCODER[2][2] = { {ENCODER_0_X,ENCODER_0_Y} , {ENCODER_1_X,ENCODER_1_Y} };
byte endstop_out[2] = { ENDSTOP_0_OUT, ENDSTOP_1_OUT };
byte endstop_in[2]  = { ENDSTOP_0_IN , ENDSTOP_1_IN  };
byte step_pin[2]  = { DRIVER0_STEP_PIN, DRIVER1_STEP_PIN };
byte dir_pin[2]  = { DRIVER0_DIR_PIN, DRIVER1_DIR_PIN };


////////////
/// VARS ///
////////////

/// variables espejo para pines de entrada
bool infast, inslow, outfast, outslow;
bool enable_drivers, enable_drivers_pipe;

#ifdef TMC262
  /// Driver controller instances
  TMC26XStepper driver[2] = {
    TMC26XStepper(MOTOR_STEPS, DRIVER0_CS_PIN, DRIVER0_DIR_PIN, DRIVER0_STEP_PIN, REST_CURRENT, RESISTOR),
    TMC26XStepper(MOTOR_STEPS, DRIVER1_CS_PIN, DRIVER1_DIR_PIN, DRIVER1_STEP_PIN, REST_CURRENT, RESISTOR)
  };
#endif

/// Gestión del bucle principal
unsigned long nowmicros = 0;
boolean wehavetime = true;

/// GESTIÓN DE MOVIMIENTO Y PASOS DE LOS MOTORES
/// Contadores de microsegundos
unsigned long t_serial = 0;
unsigned long t_vel = 0;
unsigned long t_step = 0;
unsigned long step_delay = 0;
/// Estado de los motores
bool motorIsSelected[2]         = { true , true  };
byte currentStep[2] = {0, 0};
int vel = 0;
int vel_change = 0;
int setpoint = 0;

/// Variable de debug para evaluar número de ejecuciones por segundo
unsigned long loopspersec = 0;

/// Feedback con encoder
bool encoder_state[2];
bool encoder_state_chng[2];
bool encoder_state_prev[2][2];
long int encoder_accum[2]={0,0};
bool feedback_ok;
bool encoder_debug_print = false;

void setup() {

  if (SERIAL_INTERVAL>0)
    Serial.begin(115200);

#ifdef RAMPS
  pinMode(  COMMON_MANDO  , OUTPUT  );
  digitalWrite ( COMMON_MANDO, LOW  );
#endif  
  
  /// MANDO
  pinMode(  OUT_FAST      , INPUT_PULLUP   );
  pinMode(  OUT_SLOW      , INPUT_PULLUP   );
  pinMode(  IN_SLOW       , INPUT_PULLUP   );
  pinMode(  IN_FAST       , INPUT_PULLUP   );
  pinMode(  SETA_MANDO    , INPUT_PULLUP   );
  
  /// CAJA
  pinMode(  DISABLE_MOTOR1  , DIS_MOT_INPUT_MODE   );
  pinMode(  DISABLE_MOTOR0  , DIS_MOT_INPUT_MODE   );

  /// SENSORES
  pinMode(  ENCODER_1_X    , INPUT  );
  pinMode(  ENCODER_1_Y    , INPUT  );
  pinMode(  ENCODER_0_X    , INPUT  );
  pinMode(  ENCODER_0_Y    , INPUT  );
  pinMode(  ENDSTOP_1_OUT  , INPUT  );
  pinMode(  ENDSTOP_1_IN   , INPUT  );
  pinMode(  ENDSTOP_0_OUT  , INPUT  );
  pinMode(  ENDSTOP_0_IN   , INPUT  );
  
  
  /// INICIALIZACION MOTORES
  enable_drivers_pipe = false;
  pinMode( DRIVER0_ENABLE_PIN, OUTPUT);
  pinMode( DRIVER1_ENABLE_PIN, OUTPUT);
  digitalWrite( DRIVER0_ENABLE_PIN, enable_drivers_pipe xor DRIVER_ENABLE_ACTLOW );
  digitalWrite( DRIVER1_ENABLE_PIN, enable_drivers_pipe xor DRIVER_ENABLE_ACTLOW );
  
#ifdef RAMPS
  pinMode( DRIVER0_STEP_PIN , OUTPUT);
  pinMode( DRIVER1_STEP_PIN , OUTPUT);
  pinMode( DRIVER0_DIR_PIN  , OUTPUT);
  pinMode( DRIVER1_DIR_PIN  , OUTPUT);
#endif  
#ifdef TMC262
  for (int i=0; i<2; i++){
    /// char constant_off_time, char blank_time, char hysteresis_start, char hysteresis_end, char hysteresis_decrement
    driver[i].setSpreadCycleChopper(5,24,8,7,1);
    driver[i].start();
    driver[i].enableSDOFF();
    driver[i].setSDOFFstep( currentStep[i] & 0x3 );
    driver[i].setCurrent( REST_CURRENT ); /// Not needed; set in first slowloop iteration.
  }
#endif  

}

void slowloop(){
  /// GESTION DE LA VELOCIDAD DEL MOTOR Y PROGRAMACION DEL SIGUIENTE PASO
  if(nowmicros > t_vel) {
    /// Programa la proxima ejecucion de la rutina de cambio de velocidad
    t_vel += VEL_CHANGE_INTERVAL;
    
    /// Almacena valores de pines que se utilizan varias veces en variable
    infast  = !digitalRead(  IN_FAST   );
    inslow  = !digitalRead(  IN_SLOW   );
    outfast = !digitalRead(  OUT_FAST  );
    outslow = !digitalRead(  OUT_SLOW  );
    if ( !infast && !inslow && !outfast && !outslow && (vel == 0) ) {
      /// El motor seleccionado solo se puede cambiar partiendo desde stop
      motorIsSelected[0] = ! ( digitalRead( DISABLE_MOTOR0 ) xor DIS_MOT_ACTIVE_LOW );
      motorIsSelected[1] = ! ( digitalRead( DISABLE_MOTOR1 ) xor DIS_MOT_ACTIVE_LOW );
      /// Resetear la posición de los encoder al comienzo del movimiento
      encoder_accum[0] = 0;
      encoder_accum[1] = 0;
      feedback_ok = true;
    }

    /// Fijar setpoint de velocidad en funcion de inputs
    if ( !feedback_ok )
      setpoint = 0;
    else if ( (infast || inslow) && (outfast || outslow) ) /// SE PODRIA APROVECHAR ESTA CONDICION PARA CAMBIAR EL SETPOINT_FAST, POR EJEMPLO
      setpoint = 0; 
    else if ( infast  ) setpoint =      SETPOINT_FAST ;
    else if ( outfast ) setpoint = -1 * SETPOINT_FAST ;
    else if ( inslow  ) setpoint =      SETPOINT_SLOW ;
    else if ( outslow ) setpoint = -1 * SETPOINT_SLOW ;
    else                setpoint =  0 ;
    
#ifdef ENABLE_ENDSTOPS
    for (int i=0; i<2; i++){
      if (motorIsSelected[i]){
        setpoint = constrain( setpoint, 
                              -SETPOINT_FAST * digitalRead( endstop_out[i] ),
                              SETPOINT_FAST * digitalRead( endstop_in[i]) );
      }
    }
#endif
    
    /// Fijar variacion de velocidad (vel_change) en funcion de velocidad actual y del setpoint    
    if      ( abs(vel) < abs(setpoint) ) vel_change = vel > 0 ?  1 : -1;
    else if ( abs(vel) > abs(setpoint) ) vel_change = vel > 0 ? -1*DECEL_MUL : DECEL_MUL;
    else vel_change = 0;

    /// Arranque desde stop
    if (vel == 0 && vel_change != 0) {
      /// Ejecutar primer paso en la siguiente iteracion de loop()
      t_step = nowmicros;
      
    }
    
    /// Calculo de la nueva velocidad
    /// Si en el intervalo [ -JOG, JOG ] el cambio es abrupto
    if ( (abs(vel)<JOG) && (vel_change!=0) ){
      vel = constrain(setpoint, -JOG, JOG);
#ifdef TMC262
      for (int i=0; i<2; i++) /// Ajustar corriente en caso de arranque o parada
        driver[i].setCurrent( ( vel != 0 && motorIsSelected[i] ) ? MOVE_CURRENT : REST_CURRENT );
#endif TMC262
    }
    /// Si no, variar velocidad segun lo calculado
    else vel += vel_change;

    /// Calcular el intervalo de tiempo entre pasos en microsegundos para vel en rpm
    step_delay = (60UL * 1000UL * 1000UL) / (unsigned long)MOTOR_STEPS / (unsigned long)abs(vel);
  }
  
  /// COMUNICACIÓN SERIE
  /// Se ejecuta cada SERIAL_INTERVAL segundos si SERIAL_INTERVAL > 0
  if (SERIAL_INTERVAL>0 && millis() > t_serial + SERIAL_INTERVAL*1000) 
    printDebugInfo();
  
  /// Analisis del encoder
  if(motorIsSelected[0] and motorIsSelected[1])
    encoder();
  
  /// OTRAS ACCIONES NO TIME-CRITICAL
  /// 
  
}

void loop() {
  
  /// NO INTRODUCIR NADA EN ESTE BUCLE
  /// CUALQUIER TAREA NO TIME-CRITICAL DEBERÍA IR
  /// EN LA FUNCION slowloop()
  
  nowmicros = micros();
  
  /// AVANCE DE PASO DEL MOTOR
  if (vel!= 0 && nowmicros > t_step) {
    t_step += step_delay; /// this way (instead of t_step = nowmicros+step_delay) makes the setpoint freq to be exact
#ifdef RAMPS
    for (int i=0; i<2; i++) digitalWrite( dir_pin[i], (vel>0) ? LOW : HIGH );
    delayMicroseconds(1);
    for (int i=0; i<2; i++) digitalWrite( step_pin[i], motorIsSelected[i] ? HIGH : LOW );
    delayMicroseconds(1);
    for (int i=0; i<2; i++) digitalWrite( step_pin[i], LOW );
#endif
#ifdef TMC262
    for (int i=0; i<2; i++){
      if(motorIsSelected[i]){
        currentStep[i] += ( (vel>0)? DRIVER_SIGN : -1*DRIVER_SIGN ); /// direction depends on sign(vel)
        driver[i].setSDOFFstep(currentStep[i]&0x3); /// produces the step in the motor
      }
    }
#endif
    for (int i=0; i<2; i++) encoder_accum[i] +=  motorIsSelected[i] * ( (vel>0) ? -1 : 1 ) * ENCODER_STEPS_PER_REV;
    wehavetime = true; ///enables immediate start of speed re-evaluation code
  }
  
  enable_drivers = ! digitalRead( SETA_MANDO ); /// seta mando es active HIGH: seta = 1 => seta pulsada => disable
  if(enable_drivers != enable_drivers_pipe) {
    digitalWrite( DRIVER0_ENABLE_PIN, enable_drivers xor DRIVER_ENABLE_ACTLOW );
    digitalWrite( DRIVER1_ENABLE_PIN, enable_drivers xor DRIVER_ENABLE_ACTLOW );
  }
  enable_drivers_pipe = enable_drivers;

  if (vel == 0 || wehavetime)
    slowloop();

  /// used for main loop load evaluation with serial reporting.
  /// comment for production
  loopspersec += 1; 

  /// every code that is not producing steps is not timing-critical:
  /// it can only be run immediately after an step has been produced, 
  /// in that moment we know there some margin
  wehavetime = false; 
}


void encoder() {
  for (int i=0; i<2; i++){
    for (int j=0; j<2; j++){
      encoder_state[j] = digitalRead( MOTOR_ENCODER[i][j] );
      encoder_state_chng[j] = encoder_state[j] != encoder_state_prev[i][j];
      encoder_state_prev[i][j] = encoder_state[j];
    }
    //if(encoder_debug_print){
    //  Serial.print(" encoder");
    //  Serial.print(i);
    //  Serial.print(" state ");
    //  Serial.print(encoder_state[0]);
    //  Serial.print(encoder_state[1]);
    //  Serial.print(" state_chng ");
    //  Serial.print(encoder_state_chng[0]);
    //  Serial.print(encoder_state_chng[1]);
    //}
    if( encoder_state_chng[0] != encoder_state_chng[1] ) { // one and only one sensor changed
      /// 00 -> 01 --> 11 --> 10
          //( encoder_state_chng[0] &&(encoder_state[0]!=encoder_state[1]) )  
          //|| (encoder_state_chng[1] &&(encoder_state[0]==encoder_state[1]) )
      encoder_accum[i] += GEAR_RATIO * MOTOR_STEPS * (  ( encoder_state_chng[0] == ( encoder_state[0]!=encoder_state[1] ) ) ? 1 : -1  );
    }
  }
  
//  feedback_ok = ( ( abs(encoder_accum[0]) + abs(encoder_accum[1]) ) < (ENCODER_ERROR_THRESHOLD * GEAR_RATIO * MOTOR_STEPS) );
  feedback_ok = true;
  
  if(encoder_debug_print) Serial.println( ( abs(encoder_accum[0]) + 
abs(encoder_accum[1]) ) );
  if(encoder_debug_print) Serial.print( feedback_ok );
  
  if (encoder_debug_print) Serial.println();
  encoder_debug_print = false;
}


void printDebugInfo() {
  t_serial = millis();
  //encoder_debug_print = true;
  
  Serial.print( "Endstops: D_IN, I_IN, D_OUT, I_OUT  ");
  Serial.print(  digitalRead( ENDSTOP_0_IN  ) );
  Serial.print(  digitalRead( ENDSTOP_1_IN  ) );
  Serial.print(  digitalRead( ENDSTOP_0_OUT ) );
  Serial.println(  digitalRead( ENDSTOP_1_OUT ) );
  
  //Serial.print( "DISABLE_MOTORI, DISABLE_MOTORD:  ");
  //Serial.print(  digitalRead( DISABLE_MOTOR1 ) );
  //Serial.print(  digitalRead( DISABLE_MOTOR0 )  );
  //Serial.print( "      motorIsSelected (D,I)  ");
  //Serial.print(  motorIsSelected[0] );
  //Serial.println(  motorIsSelected[1]  );
  
  
  //Serial.print( "Mando (infast, inslow, outfast, outslow, SETA):  ");
  //Serial.print( infast  );
  //Serial.print( inslow  );
  //Serial.print( outfast  );
  //Serial.print( outslow );
  //Serial.print("  ");
  //Serial.println(  digitalRead( SETA_MANDO  ) );
  
  
  
  //Serial.print("Velocity: ");
  //Serial.println(vel);
  //Serial.print("Setpoint: ");
  //Serial.println(setpoint);
  //Serial.print("CurrentStep: ");
  //Serial.println(currentStep[0]&0x3);
  
  
  //Serial.print("Motor encoder differences (D,I):  ");
  //Serial.print(encoder_accum[0]    );
  //Serial.print(" ");
  //Serial.println(encoder_accum[1]  );
  //Serial.print(encoder_accum[0] / (GEAR_RATIO * MOTOR_STEPS) );
  //Serial.print(" ");
  //Serial.println(encoder_accum[1] / (GEAR_RATIO * MOTOR_STEPS) );
  
  //Serial.println(times_zero_any_encoder);
  
  //Serial.println();
  
  //Serial.println(loopspersec);
  //loopspersec = 0;
  }
